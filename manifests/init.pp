# example class for overriding data
# To trigger this you can run pdk bundle exec rake spec_prep
# pdk bundle exec rspec spec/classes/override_spec.rb

# This is an example class of how you would utilize the functions to get the values
class override(
  $context_type = 'batt_index',
  $override_name = 'appd_machine_vers',
  $context_id = lookup('override::current_batt_index_version')
  ) {
  # lookup while enforcing the value to match a specific datatype  
  $overrides_per_node = lookup('override::data_overrides', {value_type => Override::EntryWithContext})
  # lookup of override while using a contect_type to get the value of a subkey
  $overrides_per_id = lookup('override::data_overrides.batt_index')
  # lookup of override while using a contect_type and context_id to get the value of a subkey
  $overrides_per_id_and_name = lookup("override::data_overrides.batt_index.${context_id}")
  # using multiple subkey searches we can get the value of the application override in a single line
  $overrides_per_id_and_app = lookup("override::data_overrides.batt_index.${context_id}.${override_name}")

  # Instead of remembering how to use the lookup we can call a function that not only looksup the value
  # but also allows us to extract away any details so that we can concentrate on just getting the value
  # Any changes to the function won't hurt the usage of it if the function defination stays the same.
  $tom_version = override::lookup_override($context_type, $context_id, $override_name, '1.2.3')

}
