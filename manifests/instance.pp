# This is a dummy resource used to locate when an override is used
# From the lookup_override function.
# @param context_type - the type of context
# @param context_id - the id of the context
# @param override_name - the name of the override 
# @param metadata - the value and other metadata used in the override
# @param caller - the calling class / parent or other info used to determine where
#                 the override was used.
define override::instance(
  String $context_type,
  String $context_id,
  String $override_name,
  Override::BaseEntry $metadata,
  Optional[String] $caller,
){
  # do nothing here
}
