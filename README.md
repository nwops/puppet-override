[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/nwops/puppet-override) 

# Override
Application level hiera overrides, done right.

## Description
This is a specific use case for some organizations where multiple hiera backends are used to control app versions and different groups have access to different backends.  This override module is a example module that contains patterns, functions and datatypes to allow any organization to create a hiera driven application level override capability. 

## Override Pattern

The override pattern defines a method of providing data values scoped to a context. 

## Data Structure
The data structure below is comprised of multiple subkeys so please read everything below to understand each part. 

The data structure parts are comprised of the following:
1. context type
2. context id
3. override name
4. override entry


### Override Object

An override entry is a hash structure containing at a minimum a `value` key and a `reason` key. Additional metadata fields are optional. Further required fields may be added in the future.

**Example:**

 ```yaml

value: true
reason: "demonstration of override pattern"
ticket: "OVR-10292" # <-- additional metadata

```

Some sample metadata that could go along with the override data.

```yaml
entry_date: 1587081718
expiration_date: 1588291361  # 2 weeks
description: 'Override for last minute management decision'
reason: 'Manager said so'
ticket: 'JIRA-1234' 
```

Overrides will be given names and made addressable / lookup-able by being stored in a collection.

### Override Collection

Overrides are collected together in a hash. Each override in the collection is saved with a key, which is functionally the name of the override.  Each override entry must have a name.  For example: appd_machine_vers or tomcat_app_vers. This the collection is a hash names must be unique.

**Example:**

 ```json
appd_machine_vers:
  value: '10.1'
  reason: A very good reason
  ticket: OVR-10292
tomcat_app_vers:
  value: 9.0.34
  reason: Best reason ever
  ticket: OVR-11023
```

### Override Contexts

An override collection is referenced from a context. Context is important because when the override pattern is used it is context that determines whether or not a given override collection is valid, or in scope, when Puppet is running.

Contexts should be indexable by some value available in Puppet's scope, such as `$release_id`. The following example shows two contexts. In the first context an override is present for both "appd\_machine\_vers" and for "tomcat\_app\_vers". In the second context, there is only an override present for "tomcat\_app\_vers".

The context id as we discussed above can be anything really, but must be unique and follow some sort of pattern such as dates, release versions or something with an order for your organization to easily identify and compare overrides. The `$release_id` here is just an example as release versions are easy to track and always follow a pattern. 

**Example:**

 ```yaml
2019_01_01:  # context id
  appd_machine_vers:  # override name
    value: '10.1'
    reason: A very good reason
    ticket: OVR-10292
  tomcat_app_vers:
    value: 9.0.34
    reason: Best reason ever
    ticket: OVR-11023
2019_02_01:
  tomcat_app_vers:
    value: 9.0.34
    reason: Best reason ever
    ticket: OVR-11023
```

### Context Collection types

A context collection type is used to organize override contexts, so that the _type_ of context is clear. For example, if the context relates to `group_a`, then the contexts should be stored in a collection called "group_a'.  We give the context collection type a unique name as well.

**Example:**

```yaml
group_a:
  2019_01_01:
    appd_machine_vers:
      value: '10.1'
      reason: A very good reason
      ticket: OVR-10292
    tomcat_app_vers:
      value: 9.0.34
      reason: Best reason ever
      ticket: OVR-11023
group_b:
  42:
    apache_app_vers:
      value: 2.4.43
      reason: Clear and obvious reason
      ticket: OVR-11192
```

### Storing in Hiera

A Hiera lookup key should point to a Hash of context collections.  The main key `override::data_overrides` used here
comes from this module's `override::lookup_override` function.   This key name is easily changable but we dont' recommend changing
once you already have overides in play as every override key would also need to be updated.  

**Example:**

```yaml
override::data_overrides:
  group_a:
    2019_01_01:
      appd_machine_vers:
        value: '10.1'
        reason: A very good reason
        ticket: OVR-10292
      tomcat_app_vers:
        value: 9.0.34
        reason: Best reason ever
        ticket: OVR-11023
  group_b:
    42:
      apache_app_vers:
        value: 2.4.43
        reason: Clear and obvious reason
        ticket: OVR-11192

```

## Using this module
This module comes with a single function and two datatypes.  The datatypes are only used to validate the structure
of the incoming hieradata.  Without these datatypes we can't really enforced the returned data contains the correct structure.

See the REFERENCE.md for more information around the datatypes and functions.

### Datatypes
* Override::BaseEntry 
* Override::EntryWithContext

At this time only the `Override::BaseEntry` datatype is used in the `override::lookup_override()` function. Because
we assume the hieradata meets the qualifications and has the contexts required for the lookup to occur. 

### Functions
* override::lookup_override()

To lookup an override use the `override::lookup_override()` function.

```puppet
$context_type = 'group_a'
$context_id = $release_id
$override_name = 'tomcat'
$default_value = '1.2.3'
$value = override::lookup_override($context_type, $context_id, $override_name, $default_value)
```

## Creating a wrapper function 
When you are using this module's function `override::lookup_override` you may find it useful to encapsulate (wrap) the functionality into your own function and further massage / transform the data returned.  This can easily be achieved and the example below should help you get started.  Although this function assumes that the value in the override will **ALWAYS** be a string. If this is the case than there should not be any issues, otherwise further processing will need to occur if a different datatype is returned.

```puppet
# @summary 
#  The purpose of this function is to pre-populate the context
#  if required and further encapsulate calling of the override.
#  not really needed but does provide some additional benefit if
#  the context changes or you need to mutate the returned value for any reason.
# @param context_id - the id of context to use
# @param override_name - the name of the override
# @param default_value - the value to return if no override exists
# @example 
#   override::my_override('0_29', 'tomcat', '1.2.3')
function override::my_override(
  String $context_id,
  String $override_name,
  String $default_value
) {
  $value = override::lookup_override('group_a', $context_id, $override_name, $default_value)
  $massaged_data = value.regsubst('-', '_')  # '1.3.4-pre'.regsubst('-', '_') >> "1.3.4_pre"
  return $massaged_data
}
```

## Adding metadata 
The module was created as a example only and your implementation will likely be slightly different in terms
of metadata.

Should you want to add more metadata to the override entry you will need to update the `Override::BaseEntry` datatype
to meet your requirememnts.  

Also remember that the value being returned can be of **ANY** datatype which is used in puppet code. The metadata (not the value)
is more for humans to read and understand why the override exist and when it can be removed.