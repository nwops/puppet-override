# @summary Looks up the hiera key and returns the value of the override 
#          This is similar to lookup_override with the exeception that
#          the value returned from the lookup is a json string encoded as baseentry.
# @param context_type - the context type ie. group_a
# @param context_id - the id of the context subcomponent 
# @param override_name - the name of the override to lookup
# @param default_value - the value to return if no value is supplied
# @param lookup_key_name - the name to use when looking up the overrides in hiera
# @return [Any] - Most times the value will be a string, but anything is possible
# @example Example Use
#   override::lookup_override('batt_index', '0_29' 'tomcat', '1.2.3')
function override::lookup_override_string(
  String $context_type,
  String $context_id,
  String $override_name,
  Any $default_value = undef,
  String $lookup_key_name = 'override::data_overrides',
  ) >> Any {

  # We need to turn the default value into the json string type
  $default_entry = "{\"value\": \"${default_value}\" }"
  # create the full lookup key
  $lookup_key = "${lookup_key_name}.${context_type}.${context_id}.${override_name}"

  # lookup the override and return a BaseEntry
  $returned_json_data = lookup($lookup_key_name, {value_type => String, default_value => $default_entry })
  $override_data = $returned_json_data.parsejson

  # check to see if the object returned is a valid entry with context
  # and either retrun the value or return the default value
  if $override_data =~ Override::EntryWithContext {
    $value = $override_data.dig($context_type, $context_id,$override_name, 'value')
    if $value {
      $value
    } else {
      $default_value
    }
  } else {
    $override_data['value']
  }
}
