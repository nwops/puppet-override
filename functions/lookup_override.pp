# @summary Looks up the hiera key and returns the value of the override
# @param context_type - the context type ie. group_a
# @param context_id - the id of the context subcomponent 
# @param override_name - the name of the override to lookup
# @param default_value - the value to return if no value is supplied
# @param lookup_key_name - the name to use when looking up the overrides in hiera
# @return [Any] - Most times the value will be a string, but anything is possible
# @example Example Use
#   override::lookup_override('batt_index', '0_29' 'tomcat', '1.2.3')
function override::lookup_override(
  String $context_type,
  String $context_id,
  String $override_name,
  Any $default_value = undef,
  String $lookup_key_name = 'override::data_overrides',
  ) >> Any {

  # We need to turn the default value into the BaseEntry type
  $default_entry = {
    value => $default_value
  }
  # create the full lookup key
  $lookup_key = "${lookup_key_name}.${context_type}.${context_id}.${override_name}"

  # lookup the override and return a BaseEntry
  $override_data = lookup($lookup_key, {value_type => Override::BaseEntry, default_value => $default_entry})

  # I shouldn't need to check for undef data since the lookup enforces via the datatype.
  return $override_data['value']
}
