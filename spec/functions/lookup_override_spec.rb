require 'spec_helper'

describe 'override::lookup_override' do
  let(:extra_facts) do
    {
      'certname' => 'pe-xl-core-0.puppet.vm',
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to run.with_params('batt_index', '0_29', 'tomcat', '1.3.1').and_return('234') }
      it { is_expected.to run.with_params('batt_index', '0_33', 'tomcat', '1.3.1').and_return('1.3.1') }
      it { is_expected.to run.with_params('batt_index', '0_33', 'tomcat').and_return(nil) }
      it { is_expected.to run.with_params('batt_index', '0_33', 'noexist', '1.8.0').and_return('1.8.0') }
      it { is_expected.to run.with_params('batt_index', '0_33').and_raise_error(ArgumentError) }
    end
  end
end
