require 'spec_helper'

describe 'Override::BaseEntry' do
  let(:valid) do
    { '0_28' =>
      { 'appd_machine_vers' => { 'value' => '123', 'reason' => 'because' },
        'tomcat' => { 'value' => '123', 'reason' => 'because' } } }
  end

  let(:invalid) do
    {
      'version' => :undef,
      'entry_date' => 1_587_081_718,
      'expiration_date' => 1_588_291_361, # 2 weeks
    }
  end

  it do
    is_expected.to allow_value(valid['0_28']['tomcat'])
  end

  it { is_expected.not_to allow_value(invalid) }
end
