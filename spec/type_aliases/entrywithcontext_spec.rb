require 'spec_helper'

describe 'Override::EntryWithContext' do
  let(:valid) do
    { 'batt_index' =>
      { '0_28' =>
        { 'appd_machine_vers' => { 'value' => '123', 'reason' => 'because' },
          'tomcat' => { 'value' => '123', 'reason' => 'because' } },
        '0_29' =>
        { 'appd_machine_vers' => { 'value' => '123', 'reason' => 'because' },
          'tomcat' => { 'value' => '234', 'reason' => 'because' } } } }
  end

  let(:invalid) do
    {
      'version' => :undef,
      'entry_date' => 1_587_081_718,
      'expiration_date' => 1_588_291_361, # 2 weeks
    }
  end

  it { is_expected.to allow_value(valid) }
  it { is_expected.not_to allow_value(invalid) }
end
