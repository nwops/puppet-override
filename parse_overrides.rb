require 'yaml'
require 'json'
require 'table_print'

# @return [Array[Hash]] - a Array of the overrides
def make_table(name, data)
  override_table = []
  data.each do |type, all_overrides|
    all_overrides.each do |id, override_data|
      override_data.each do |override_name, entry|
        override_table << {
          node: name,
          context_type: type,
          context_id: id,
          override_name: override_name,
          value: entry.delete("value"),
          reason: entry.delete("reason"),
          metadata: entry
        }
      end
    end
  end  
  override_table
end


data = YAML.load_file('index.yaml')
table_data = data.map do |name, hieradata|
  next unless hieradata.has_key?("override::data_overrides")
  override = hieradata.fetch("override::data_overrides")
  make_table(name, override)
end.compact.flatten


tp table_data
File.write('parsed_table_output.txt',TablePrint::Printer.table_print(table_data))
File.write('parsed_output.json',JSON.pretty_generate(table_data))