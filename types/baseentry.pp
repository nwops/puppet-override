# This is a datatype used to enforce incoming 
# override data. The datatype you come up with will may look
# different but should follow this same structure
# at the very least the data should be something like
# {
#   "value" => "123"
#   "reason" => "manager waved his finger"
# }
type Override::BaseEntry = Struct[{
  value => Any,
  entry_date => Optional[Integer],
  reason => Optional[String],
  description => Optional[String],
  ticket => Optional[String]
}]
