# This type is used to enforce the context of the override
# The Base entry enforces the actual content of the entry
# while this datatype enforces the context aware dataset
# 
# Example hieradata:
# override::data_overrides: # <-- hiera lookup key for overrides
#  batt_entry:  # <--- this is the context type 
#   '0_28':  # <-- context entry id (BaseEntry)
#     appd_machine_vers:  # <-- override name
#       value: '123'  # ? what type goes here
#       reason: because
#     tomcat:
#       value: '123'  # ? what type goes here
#       reason: because
type Override::EntryWithContext = Hash[String, Hash[String, Hash[String,Override::BaseEntry]]]
